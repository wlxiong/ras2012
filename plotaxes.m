function [cumleng ax1 ax2]= plotaxes(edge, leng)
% plot the track edges

% track labels
startnode = num2cell(edge(:,1));
endnode = num2cell(edge(:,2));
startlabel = cellfun(@num2str, startnode, 'UniformOutput', false);
endlabel = cellfun(@num2str, endnode, 'UniformOutput', false);
startlabel = [startlabel(:); {''}];
endlabel = [{''}; endlabel(:)];
% the minimal display space is 5.0
minlen = 5;
leng(leng<minlen) = minlen;
% cummulative track length
cumleng = cumsum([0;leng(:)]);
% plot the axes
ax1 = gca;
line(cumleng, 0, 'Color', 'k')
set(ax1,'XTick', cumleng)
set(ax1,'XTickLabel', startlabel)
ax2 = axes('Position',get(ax1,'Position'),...
           'XAxisLocation','top',...
           'YAxisLocation','right',...
           'Color','none',...
           'XColor','k','YColor','k');
line(cumleng, 0, 'Color', 'k', 'Parent', ax2)
set(ax2,'XTick', cumleng)
set(ax2,'XTickLabel', endlabel)
axes(ax1)
