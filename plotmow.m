function plotmow(edge, cumleng, mowedge, mowtime)
% plot MOW track window

    function drawrectangle(x1, x2, y1, y2)
        line([x1;x2], [y1;y1])
        line([x1;x2], [y2;y2])
        line([x1;x1], [y1;y2])
        line([x2;x2], [y1;y2])
    end

for i = 1:size(mowedge,1)
    j = find(edge(:,1)==mowedge(i,1) & edge(:,2)==mowedge(i,2));
    x1 = cumleng(j);
    x2 = cumleng(j+1);
    t1 = mowtime(i,1);
    t2 = mowtime(i,2);
    drawrectangle(x1, x2, t1, t2)
end
end
