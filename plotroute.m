function plotroute(cumleng, route, atime, dtime, orig, dest)

% cummulative track length
leng1 = cumleng(1:end-1)';
leng2 = cumleng(2:end)';
% route-specified points
if orig < dest
    ax = leng1(route==1);
    dx = leng2(route==1);
else
    ax = (leng2(route==1));
    dx = (leng1(route==1));
end
at = atime(route==1);
dt = dtime(route==1);
% line sections
xx = [ax;dx];
yy = [at;dt];
% plot the route
line(xx, yy, 'Marker', 'o')
