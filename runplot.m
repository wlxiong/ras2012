% import solution
run ./sol.m
edge = E(:,1:2);
leng = E(:,3);
mowedge = MOW(:,1:2);
mowtime = MOW(:,3:4);

% plot schedules
figure; hold on; grid on
[cumleng ax1 ax2] = plotaxes(edge, leng);
plotmow(edge, cumleng, mowedge, mowtime);
for i = 1:size(a,1)
    route = y(i,:,3);
    atime = a(i,:,4);
    dtime = d(i,:,4);
    orig = OD(i,1);
    dest = OD(i,2);
    plotroute(cumleng, route, atime, dtime, orig, dest)
end
% set Y tick and label
yy = get(ax1, 'YLim');
set(ax1,'YLim', [min(yy)-10, max(yy)+10])
set(ax2,'YLim', get(ax1, 'YLim'))
set(ax2,'YTick', get(ax1, 'YTick'))
